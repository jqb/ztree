<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>ztree-test</title>
	<link type="text/css" rel="stylesheet" href="css/ztree/zTreeStyle.css">
	<script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
	<script type="text/javascript" src="js/jquery.ztree.core-3.5.min.js"></script>

	<script type="text/javascript">
		$(function(){
			var datas = [
	         	 {id:1, pId:0, name:"root1"},
	         	 {id:2, pId:1, name:"node1"},
	         	 {id:3, pId:1, name:"node2"},
	         	 {id:4, pId:2, name:"node1_1"},
	         	 {id:5, pId:0, name:"root2"},
	         	 {id:6, pId:5, name:"node3"},
	         	 {id:7, pId:5, name:"node4"},
             ];
			var setting = {
				data: {
					simpleData: {
						enable: true,
						idKey: "id",
						pIdKey: "pId",
						rootPId: 0,
					},
				},
				view: {
					dblClickExpand: false,
					selectedMulti: false
				},
				callback: {
					onClick: zTreeOnClick
				}
			};
			function zTreeOnClick(event, treeId, treeNode){
				   alert(treeNode.tId + ", " + treeNode.name);
			}
			
			$.fn.zTree.init($("#tree"), setting, datas);
		});
	</script>
</head>
<body>
	<h1>zTree-Test</h1>
	<div id="tree" class="ztree"></div>
</body>
</html>